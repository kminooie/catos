# API for FISH

## Screen FISH.getScreen()

Get Screen instance the shell using.

## FISH.clearScreen()

Clear screen.

## FISH.clearBuffer()

Clear text buffer.

## FISH.print(msg, [color,] [bg_color])

Parameters:
```
string msg - The message you want to print
table color - Font color
table bg_color - Font background color
```

Print text to the shell

## FISH.println(msg, [color,] [bg_color])

## FISH.error(msg)

## FISH.warning(msg)

## bool FISH.runProgram(path, args)

## FISH.executeCmd(text)
