-- Templete for UI Elements
local Color = require("/ui/common.lua")

UIElement = {
    visable = true,
    x = 0,
    y = 0,
    rel_x = 0,
    rel_y = 0,
    width = 0,
    height = 0,
    rel_width = 0,
    rel_height = 0,
    foreground_color = Color:new(),
    background_color = Color:new(),
    -- Events
    OnMouseDown = function (x, y)
        error("Attempt to call OnMouseDown() without implementation.")
    end,
    OnMouseUp = function (x, y)
        error("Attempt to call OnMouseUp() without implementation.")
    end
}

--Constructor
function UIElement:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function UIElement:draw()
    error("Attempt to call draw() without implementation.")
end

return UIElement