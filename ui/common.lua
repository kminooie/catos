-- Common classes

Color = setmetatable({
    -- members
    r = 0,
    g = 0,
    b = 0,
    a = 0,
},
{
    -- meta functions override
    __call = function (me, _r, _g, _b, _a)
        -- setter
        me.r = _r or me.r
        me.g = _g or me.g
        me.b = _b or me.b
        me.a = _a or me.a
        -- getter
        if not (_r or _g or _b or _a) then return {me.r, me.g, me.b, me.a} end
    end,

    __eq = function (me, other)
        return me.r == other.r and 
                me.g == other.g and 
                me.b == other.b and 
                me.a == other.a
    end,

    __tostring = function (me)
        return string.format("{ %1f, %1f, %1f, %1f }", me.r, me.g, me.b, me.a)
    end
    
})

function Color:new(o, r, g, b, a)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    self.r = r or 0
    self.g = g or 0
    self.b = b or 0
    self.a = a or 0
    return o
end

function Color:toHex()
    return string.format("%2x%2x%2x%2x", self.r*255, self.g*255, self.b*255, self.a*255)
end

return Color