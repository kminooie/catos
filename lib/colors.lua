-- Predefined Colors

Colors = {
    -- come from https://www.webnots.com/vibgyor-rainbow-color-codes/
    Red = {1, 0, 0, 1},
    Orange = {1, 0.498, 0, 1},
    Yellow = {1, 1, 0, 1},
    Green = {0, 1, 0, 1},
    Blue = {0, 0, 1, 1},
    DarkPurple = {0.294, 0, 0.509, 1},
    Purple = {0.580, 0, 0.827, 1}
    -- TODO: more colors
}