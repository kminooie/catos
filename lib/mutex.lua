Mutex = {
    val = false
}

function Mutex:new()
    obj = {}
    setmetatable(obj, self)
    self.__index = self
    self.val = false
    return obj
end

function Mutex:acquire()
    while self.val == true do
        coroutine.yield()
    end
    self.val = true
end

function Mutex:release()
    -- throw error if try to release twice
    if self.val == false then computer.panic("Attempt to release mutex twice!") end
    self.val = false
end

return Mutex