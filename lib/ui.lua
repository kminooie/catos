-- WIP
UI = {}

function UI.setHightlight(bool)
    if bool then
        gpu:setForeground(SysConfig)
        return
    end
end

function UI.setFontColor(color, bg)
    self.gpu:setForeground(color[1], color[2], color[3], color[4])
    if bg then self.gpu:setForeground(bg[1], bg[2], bg[3], bg[4]) end
end

function UI.printCenterText(y, text)
    local screen_size = gpu:getSize()
    gpu:setText(math.floor(screen_size[1] / 2) - math.floor(#text/2), y, text)
end

function UI:validPos(x, y)
    local screen_size = gpu:getSize()
    if x < 1 or y < 1 or x > screen_size[1] or y > screen_size[2] then
        return false
    end
    return true
end

function UI:validColor(color)
    if color[1] < 0 or color[2] < 0 or color[3] < 0 or color[4] < 0 
    or color[1] > 1 or color[2] > 1 or color[3] > 1 or color[4] > 1 then
        return false
    end
    return true
end

--
-- Graphics (process bar, plot, lines, rect)
--
function UI.plot(x, y, color)
    if not UI.validColor(color) then return end
    UI.setFontColor(color)
    gpu:setText(x, y, " ")
    UI.setFontColor(SysConfig.ui_colors.font.normal)
end

function UI.line(x1, y1, x2, y2, color)
    if not UI.validPos(x1, y1) or not UI.validPos(x2, y2) then return end
    if x1 > x2 then
        temp = x1
        x1 = x2
        x2 = temp
    end
    if y1 > y2 then
        temp = y1
        y1 = y2
        y2 = temp
    end
    -- Bresenham's line algorithm
    local delta_x = x2 - x1
    local delta_y = y2 - y1
    -- vertical line
    if delta_x == 0 then
        for y = y1, y2 do
            UI.plot(x1, y, color)
        end
    else
        local delta_err = math.abs(delta_y / delta_x)
        local error = 0.0
        local y = y1
        for x = x1, x2 do
            UI.plot(x, y, color)
            error = error + delta_err
            if error >= 0.5 then
                y = y + -delta_y
                error = error - 1
            end
        end
    end
end

--
-- Dialogs (confirmation, selection, yes/no)
--
function UI.createListMenu(title, choices)
    -- params: choices
    -- return: index of selected item
    current_sel = 1
    pressed = false
    while true do
        event.listen(gpu)
        e, s, char, code, btn = event.pull(0.1)
        if e == nil then corotine.yield() end
        -- Draw UI
            -- Draw title
        UI.printCenterText(0, "=== "..title.." ===")
            -- Draw elements
        for idx, ele in pairs(choices) do
            UI.setHightlight(idx == current_sel)
            self.gpu:setText(math.floor(self.screen_size[1] / 4), idx+1, ele)
        end
            -- Draw control
        UI.setHightlight(true)
        UI.printCenterText(self.screen_size[2]-1, "UP: Previous item | Down: Next item | Enter: Confirm")
        self.gpu:flush()
        -- Get keystoke
        if e == "OnKeyUp" then
            pressed = true
        elseif pressed and e == "OnKeyDown" then
            if char == ENTER_KEYCODE then
                event.ignoreAll()
                break
            elseif code == UP_KEYCODE then
                if current_sel > 1 then
                    current_sel = current_sel -1
                end
            elseif code == DOWN_KEYCODE then
                if current_sel < #choices then
                    current_sel = current_sel + 1
                end
            end
            pressed = false
        end
        computer.skip()
    end

    UI.setHightlight(false)
    self.gpu:fill(0, 0, self.screen_size[1], self.screen_size[2], " ")
    self.gpu:flush()
    return current_sel
end

--
--page management (add page, moveto page, nav)
--

--
-- interact (buttons, checkbox, pullup bar)
--